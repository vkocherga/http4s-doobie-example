**Run dependencies**
```
docker-compose up --force-recreate -d
```

**Run application**
```
sbt run
``` 

**Get a new joke**
```
curl -XGET 'http://127.0.0.1:8080/jokes/joke'
```

**Get a history**
```
curl -XGET 'http://127.0.0.1:8080/jokes/history'
```
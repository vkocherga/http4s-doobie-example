package com.spotcap.jokes

import cats.effect.Sync
import cats.syntax.show._
import org.http4s.Method.GET
import org.http4s._
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl

case class ExternalJokeTracedClient[F[_]: Sync](client: Client[F]) {

  private val dsl = new Http4sClientDsl[F] {}
  import dsl._

  private val logger = Logger.initTraced[F](getClass)

  def get: TracedT[F, Joke] =
    for {
      _    <- logger.debug("Fetching joke from external source")
      resp <- lift_(client.expect[Joke](GET(uri"https://icanhazdadjoke.com/")))
      _    <- logger.debug(s"Received response: ${resp.show}")
    } yield resp

}

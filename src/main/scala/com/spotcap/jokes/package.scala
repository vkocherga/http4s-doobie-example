package com.spotcap

import cats.data.ReaderT
import cats.effect.{Async, Sync}

package object jokes {

  type TracedT[F[_], B] = ReaderT[F, TraceContext, B]

  def lift_[F[_], B](f: F[B]): TracedT[F, B] = ReaderT { _ =>
    f
  }

  def lift[F[_], B](f: TraceContext => F[B]): TracedT[F, B] = ReaderT { tc =>
    f(tc)
  }

  def liftSync[F[_], B](f: TraceContext => B)(implicit F: Sync[F]): TracedT[F, B] = ReaderT { tc =>
    F.pure(f(tc))
  }

  def liftAsync[F[_], B](f: TraceContext => B)(implicit F: Async[F]): TracedT[F, B] = ReaderT { tc =>
    F.delay(f(tc))
  }

}

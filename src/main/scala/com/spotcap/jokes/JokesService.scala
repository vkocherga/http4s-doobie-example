package com.spotcap.jokes

import cats.~>
import cats.data.ReaderT
import cats.effect.Sync
import cats.syntax.functor._
import cats.syntax.flatMap._
import cats.syntax.show._

case class JokesService[G[_], F[_]: Sync](
  jokesRepository: JokesRepository[G],
  transactor: G ~> F,
  client: ExternalJokeTracedClient[F]
) {

  private val logger = Logger.initTraced[F](getClass)

  def get: ReaderT[F, TraceContext, Joke] =
    for {
      _    <- logger.debug("Getting a new joke!")
      joke <- client.get
      _    <- jokesRepository.put(joke).mapK(transactor)
    } yield joke

  def history: ReaderT[F, TraceContext, List[Joke]] =
    for {
      _       <- logger.debug("Getting jokes history")
      history <- jokesRepository.jokesHistory().mapK(transactor)
    } yield history

}

object JokesService {

  def apply[G[_], F[_]](implicit ev: JokesService[G, F]): JokesService[G, F] =
    ev

}

package com.spotcap.jokes

import cats.effect._
import cats.implicits._
import doobie.free.connection.ConnectionIO
import doobie.util.ExecutionContexts
import io.circe.config.parser
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.{Router, Server}

object Main extends IOApp {

  def createServer[F[_]: ContextShift: ConcurrentEffect: Timer]: Resource[F, Server[F]] =
    for {
      dbConfig <- Resource.liftF(
                   parser.decodePathF[F, DatabaseConfig]("db-config")
                 )
      connEc   <- ExecutionContexts.fixedThreadPool[F](1)
      txnEc    <- ExecutionContexts.cachedThreadPool[F]
      clientEc <- ExecutionContexts.cachedThreadPool[F]
      xa <- DatabaseConfig.dbTransactor(
             dbConfig,
             connEc,
             Blocker.liftExecutionContext(txnEc)
           )
      jokesRepository = PostgresJokesRepository
      client          <- BlazeClientBuilder[F](clientEc).resource
      tracedClient    = ExternalJokeTracedClient(client)
      jokesService    = JokesService[ConnectionIO, F](jokesRepository, xa.trans, tracedClient)
      httpApp = Router(
        "/jokes" -> Http4sJokesEndpoints(jokesService).jokesRoutes()
      ).orNotFound
      _ <- Resource.liftF(DatabaseConfig.initializeDb(dbConfig))
      server <- BlazeServerBuilder[F]
                 .bindHttp(8080, "0.0.0.0")
                 .withHttpApp(httpApp)
                 .resource
    } yield server

  def run(args: List[String]): IO[ExitCode] =
    createServer.use(_ => IO.never).as(ExitCode.Success)
}

package com.spotcap.jokes

import cats.data.ReaderT

trait JokesRepository[F[_]] {

  def put(joke: Joke): ReaderT[F, TraceContext, Unit]

  def jokesHistory(): ReaderT[F, TraceContext, List[Joke]]

}

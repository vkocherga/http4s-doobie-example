package com.spotcap.jokes

import java.util.UUID

import cats.Show

case class TraceContext(uuid: UUID)

object TraceContext {
  implicit val show: Show[TraceContext] = Show.fromToString
}

package com.spotcap.jokes

import java.util.UUID

import cats.effect.Sync
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import cats.syntax.flatMap._
import org.http4s.circe.CirceEntityCodec._

case class Http4sJokesEndpoints[G[_], F[_]: Sync](
  jokesService: JokesService[G, F]
) {

  def jokesRoutes(): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    HttpRoutes.of[F] {
      case GET -> Root / "joke" =>
        jokesService.get.run(TraceContext(UUID.randomUUID())).flatMap(Ok(_))
      case GET -> Root / "history" =>
        jokesService.history.run(TraceContext(UUID.randomUUID())).flatMap(Ok(_))
    }
  }
}

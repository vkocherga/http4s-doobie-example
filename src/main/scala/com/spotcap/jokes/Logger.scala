package com.spotcap.jokes

import cats.data.ReaderT
import cats.effect.Sync
import io.chrisdavenport.log4cats.{ Logger => Log4CatsLogger }
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import cats.syntax.show._

object Logger {

  def init[F[_]: Sync](classz: Class[_]): Log4CatsLogger[F] = Slf4jLogger.getLoggerFromClass(classz)

  def initTraced[F[_]: Sync](classz: Class[_]): Log4CatsLogger[ReaderT[F, TraceContext, *]] = {

    val logger = Slf4jLogger.getLoggerFromClass[F](classz)

    new Log4CatsLogger[ReaderT[F, TraceContext, *]] {
      override def error(t: Throwable)(message: => String): ReaderT[F, TraceContext, Unit] = ???

      override def warn(t: Throwable)(message: => String): ReaderT[F, TraceContext, Unit] = ???

      override def info(t: Throwable)(message: => String): ReaderT[F, TraceContext, Unit] = ???

      override def debug(t: Throwable)(message: => String): ReaderT[F, TraceContext, Unit] = ???

      override def trace(t: Throwable)(message: => String): ReaderT[F, TraceContext, Unit] = ???

      override def error(message: => String): ReaderT[F, TraceContext, Unit] = ???

      override def warn(message: => String): ReaderT[F, TraceContext, Unit] = ???

      override def info(message: => String): ReaderT[F, TraceContext, Unit] = ???

      override def debug(message: => String): ReaderT[F, TraceContext, Unit] =
        lift(tc => logger.debug(s"[tc: ${tc.show}] $message"))

      override def trace(message: => String): ReaderT[F, TraceContext, Unit] = ???
    }
  }
}

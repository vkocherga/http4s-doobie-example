package com.spotcap.jokes

import cats.data._
import cats.syntax.show._
import cats.syntax.functor._
import doobie._
import doobie.implicits._

object PostgresJokesRepository extends JokesRepository[ConnectionIO] {

  private val logger = Logger.initTraced[ConnectionIO](getClass)

  def put(joke: Joke): ReaderT[ConnectionIO, TraceContext, Unit] =
    for {
      _ <- logger.debug(s"Going to insert joke: ${joke.show}")
      _ <- lift { tc =>
            sql"""INSERT INTO jokes(message, tc) values (${joke.joke}, ${tc.show})""".update
              .withUniqueGeneratedKeys[Long]("id")
              .void
          }
    } yield ()

  def jokesHistory(): ReaderT[ConnectionIO, TraceContext, List[Joke]] =
    lift_(sql"""SELECT message FROM jokes""".query[Joke].to[List])

}
